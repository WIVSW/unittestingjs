const chai = require('chai');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');
chai.use(sinonChai);

const { expect } = chai;

const { Player } = require('../src/domain/player');
const { RollDiceGame, Bet } = require('../src/domain/game');
const { Create } = require('../test/DSL/create');
const MIN_AMOUNT = 1;

describe('Player', () => {
    let game;
    let player;
    
    beforeEach(() => {
        player = new Player();
    });

    describe('joins', () => {
        beforeEach(() => {
            game = new RollDiceGame();
             player = Create
                .player()
                .withChips(MIN_AMOUNT)
                .inGame(game)
                .build();
        });

        it('new game successfully', () => {
            expect(player.activeGame()).to.be.equals(game);
        });
        
        
        it('second game receive error', () => {
            const game2 = new RollDiceGame();

            expect(() => player.joins(game2)).to.throw();
        });
    
    });
    
    describe('buy', () => {
        it('add chips equals amount', () => {
            expect(player.getAvailableChips()).to.be.equals(MIN_AMOUNT);
        });
    })
});


describe('RollDiceGame', () => {
    let game;
    
    beforeEach(() => {
        game = new RollDiceGame();
    });
    
    
    describe('play', () => {
        const SCORE = 1;
        const INVALID_SCORE = 2;
        const AMOUNT = 1;
        const MULTIPLAY = 6;
        
        let player;
        let bet;
        let invalidBet;
        let randomStub;
    
        let winStub;
        let loseStub;
        
        beforeEach(() => {
            player = Create
                .player()
                .withChips(MIN_AMOUNT)
                .inGame(game)
                .withBet(AMOUNT, INVALID_SCORE)
                .build();

            randomStub = sinon.stub(Math, 'random');
            winStub = sinon.stub(player, 'win');
            loseStub = sinon.stub(player, 'lose');
    
            randomStub.returns(0);
        });
        
        afterEach(() => {
            randomStub.restore();
            winStub.restore();
            loseStub.restore();
        });
        
        it('when player wins call wins method with multiplied amount', () => {
            game.play();
            
            expect(winStub).to.be.calledWith(AMOUNT * MULTIPLAY);
        });
    
        it('when score is different method lose will be called for all losed players', () => {
            const player2 = Create
                .player()
                .withChips(MIN_AMOUNT)
                .inGame(game)
                .withBet(AMOUNT, 3)
                .build();
            const loseStub2 = sinon.stub(player2, 'lose');

            game.play();
        
            expect(loseStub).to.be.called;
            expect(loseStub2).to.be.called;
        });
    });
});