const { Player } = require('../../src/domain/player');
const { Game } = require('../../src/domain/game');
const { Bet } = require('../../src/domain/game');

let player = null
let game = null;

class Create {
    static player() {
        player = new Player();
        return {
            withChips(chips) {
                player.buy(chips);
                return this
            },
            build() {
                return player
            },
            inGame(game) {
                player.joins(game);
                return this
            },
            withBet(amount, score) {
                const bet = new Bet(amount, score);
                player.bet(bet);
                return this;
            }
        };
    }
}

module.exports = { Create }