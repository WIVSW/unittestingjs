class Bet {
    #score;
    #amount;

    constructor(amount, score) {
        this.#score = score;
        this.#amount = amount;
    }

    score() {
        return this.#score
    }

    amount() {
        return this.#amount
    }

}

class RollDiceGame {
    #bets = new Map();

    addBet(player, bet) {
        this.#bets.set(player, bet)
    }

    play() {
        const winningScore = Math.floor(Math.random() * 6) + 1

        this.#bets.forEach((bet, player) => {
            if (bet.score() === winningScore) {
                player.win(bet.amount() * 6)
            } else {
                player.lose()
            }
        })
    }

    leave(player) {
        delete (this.#bets[player]);
    }
}

module.exports = { RollDiceGame, Bet }